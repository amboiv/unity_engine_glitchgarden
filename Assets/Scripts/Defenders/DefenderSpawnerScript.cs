﻿using UnityEngine;
using System.Collections;

public class DefenderSpawnerScript : MonoBehaviour
{
	public Camera cam;
	private GameObject defenderParent;
	private StarDisplayScript starDisplay;

	// Use this for initialization
	void Start ()
	{
		starDisplay = GameObject.FindObjectOfType <StarDisplayScript> ();
		defenderParent = GameObject.Find ("Defenders");

		if (!defenderParent)
		{
			defenderParent = new GameObject("Defenders");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnMouseDown ()
	{
		Vector2 instantiateDefenderAtSquare = SnapToGrid (CalculateWorldPointOfMouseClick ());
		GameObject defender = ButtonScript.SelectedGameObject;

		if (defender)
		{
			int defenderCost = defender.GetComponent<DefenderScript>().StarCost;

			if (starDisplay.UseCurrency (defenderCost) == StarDisplayScript.Status.SUCCESS)
			{
				GameObject newDefender = (GameObject) Instantiate (defender, instantiateDefenderAtSquare, Quaternion.identity);
				newDefender.transform.parent = defenderParent.transform;
			}
			else
			{
				Debug.Log ("Not enough stars to place object.");
			}
		}
	
		

	}

	Vector2 CalculateWorldPointOfMouseClick ()
	{
		float mouseX = Input.mousePosition.x;
		float mouseY = Input.mousePosition.y;
		float distanceFromCamera = 10f;

		Vector3 weirdTriplet = new Vector3(mouseX, mouseY, distanceFromCamera);
		Vector2 worldPos = cam.ScreenToWorldPoint (weirdTriplet);

		return worldPos;
	}

	Vector2 SnapToGrid (Vector2 rawWorldPos)
	{
		Vector2 roundedWordlPos = new Vector2(Mathf.Round(rawWorldPos.x), Mathf.Round (rawWorldPos.y));
		return roundedWordlPos;
	}
}
