﻿using UnityEngine;

public class ShootersScript : MonoBehaviour
{
	public GameObject Projectile;

	private GameObject ProjectileParent;
	private Animator anim;
	private EnemySpawnerScript sameLaneSpawner;

	// Use this for initialization
	void Start ()
	{
		anim = GetComponent <Animator> ();

		ProjectileParent = GameObject.Find ("Projectiles");
		if (!ProjectileParent)
		{
			ProjectileParent = new GameObject("Projectiles");
		}

		SetMyLaneSpawner ();

	}

	// Update is called once per frame
	void Update ()
	{
		if (IsAttackerAheadInLane ())
		{
			anim.SetBool ("IsAttacking", true);
			
		}
		else
		{
			anim.SetBool ("IsAttacking", false);
		}
	}

	bool IsAttackerAheadInLane ()
	{
		if (sameLaneSpawner.transform.childCount <= 0)
		{
			return false;
		}


		foreach (Transform attacker in sameLaneSpawner.transform)
		{
			if (attacker.transform.position.x >= transform.position.x)
			{
				return true;
			}
		}
		return false;
	}

	void SetMyLaneSpawner ()
	{
		EnemySpawnerScript[] enemySpawners = GameObject.FindObjectsOfType <EnemySpawnerScript> ();

		foreach (var spawner in enemySpawners)
		{
			if (spawner.transform.position.y == transform.position.y)
			{
				sameLaneSpawner = spawner;
				return;
			}
		}
		Debug.LogError (name + " can't find spawner in this lane.");
	}

	private void Fire ()
	{
		if (sameLaneSpawner.transform.childCount > 0)
		{
			Transform firstAttackerInLane = sameLaneSpawner.transform.GetChild (0);
			GameObject projectileInstance = Instantiate (Projectile);
			projectileInstance.transform.parent = ProjectileParent.transform;
			projectileInstance.transform.position = transform.position;
			projectileInstance.GetComponent<ProjectileScript> ().SetFirstTargetInLane (firstAttackerInLane);
		}
		
	}
}
