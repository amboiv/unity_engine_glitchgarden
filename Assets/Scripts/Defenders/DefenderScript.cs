﻿using UnityEngine;
using System.Collections;

public class DefenderScript : MonoBehaviour
{
	public int StarCost = 50;

	private StarDisplayScript starDisplay;


	// Use this for initialization
	void Start ()
	{
		starDisplay = GameObject.FindObjectOfType <StarDisplayScript> ();
	}

	// Update is called once per frame
	void Update ()
	{
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		Debug.Log (other + "trigger enter");
	}

	public void AddCurrency (int amount)
	{
		starDisplay.AddCurrency (amount);
	}
}
