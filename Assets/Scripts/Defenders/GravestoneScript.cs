﻿using UnityEngine;
using System.Collections;

public class GravestoneScript : MonoBehaviour
{
	private Animator anim;
	// Use this for initialization
	void Start ()
	{
		anim = GetComponent <Animator> ();
	}

	// Update is called once per frame
	void Update ()
	{

	}

	void OnTriggerStay2D (Collider2D other)
	{
		AttackerScript attacker = other.gameObject.GetComponent<AttackerScript>();

		if (attacker && !other.GetComponent <FoxScript> ())
		{
			anim.SetTrigger ("UnderAttackTrigger");
		}
	
	}
}
