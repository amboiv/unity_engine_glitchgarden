﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour
{
	public float Damage = 50f;
	private float speed;
	private Transform firstTargetInLane;
	private Animator anim;
	private float animationLength;
	private float distanceToTarget;


	// Use this for initialization
	void Start ()
	{
		anim = GetComponent <Animator> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (firstTargetInLane)
		{
			MoveTowardsTarget ();
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		AttackerScript attacker = other.gameObject.GetComponent <AttackerScript> ();
		HealthScript attackerHealth = other.gameObject.GetComponent <HealthScript> ();

		if (attackerHealth && attacker && IsAtSameHeight(other.transform))
		{
			attackerHealth.TakeDamage (Damage);
			Destroy (gameObject);
		}
	}

	public float GetDamage ()
	{
		return Damage;
	}

	public void Hit ()
	{
		Destroy (gameObject);
	}

	private void MoveTowardsTarget ()
	{

		//Get length of the projectile animation
		animationLength = anim.GetCurrentAnimatorStateInfo (0).length;

		//Movement speed (to be used per frame) of the projectile is set to be 
		//the distance to the target divided by the animation duration - the movement speed of the attacker 
		speed = distanceToTarget / animationLength - firstTargetInLane.GetComponent<AttackerScript>().CurrentSpeed;
		transform.position = Vector3.MoveTowards (transform.position, firstTargetInLane.position, speed * Time.deltaTime);
	}

	//Need this method because the collider of the projectiles only travel along the x-axis.
	//The Collision event shouldn' occur until the axe's body and the target are at the same Y level.
	private bool IsAtSameHeight (Transform target)
	{
		Transform axeTransform = transform.Find ("Body");

		return axeTransform.position.y <= target.position.y;
	}

	public void SetFirstTargetInLane (Transform firstInLane)
	{
		firstTargetInLane = firstInLane;

		//Sets the distance to the target Each time the Fire event is triggerd on the shooters animation.
		distanceToTarget = firstTargetInLane.position.x - transform.position.x;
	}
}
