﻿using UnityEngine;
using System.Collections;

public class HealthScript : MonoBehaviour
{
	public float Health = 100f;

	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void TakeDamage (float damage)
	{
		Health -= damage;
		if (Health <= 0)
		{
			//Optionally trigger die animation.
			DestroyObject ();
		}
	}

	void DestroyObject ()
	{
		Destroy (gameObject);
	}
}
