﻿using UnityEngine;
using System.Collections;

public class MusicPlayerScript : MonoBehaviour
{
	public AudioClip[] musicClips;

	private AudioSource bgMusic;

	// Use this for initialization
	void Awake ()
	{
			DontDestroyOnLoad (gameObject);	
	}

	void Start ()
	{
		bgMusic = GetComponent <AudioSource> ();
		bgMusic.volume = PlayerPrefsManagerScript.GetMasterVolume ();
	}

	//TODO: Make function relevant	
	void OnLevelWasLoaded (int level)
	{
		AudioClip levelClip = musicClips[level];
		Debug.Log ("MusicPlayer: loaded level " + level);

		if (levelClip)
		{
			bgMusic.clip = levelClip;
			bgMusic.loop = true;
			bgMusic.Play ();
		}
	}

	public void ChangeVolume (float volume)
	{
		bgMusic.volume = volume;
	}

	// Update is called once per frame
	void Update ()
	{
	}
}
