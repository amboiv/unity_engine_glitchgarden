﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerPrefsManagerScript : MonoBehaviour
{
	private const string MASTER_VOLUME_KEY = "MasterVolume";
	private const string DIFFUCULTY_KEY = "Difficulty";
	private const string LEVEL_KEY = "LevelUnlocked";

	public static void SetMasterVolume (float volume)
	{
		if (volume >= 0f && volume <= 1f)
		{
			PlayerPrefs.SetFloat (MASTER_VOLUME_KEY, volume);
		}
		else
		{
			Debug.LogError ("Master Volume out of range");
		}	
	}

	public static float GetMasterVolume ()
	{
		return PlayerPrefs.GetFloat (MASTER_VOLUME_KEY);
	}

	public static void UnlockLevel (int level)
	{
		if (level <= SceneManager.sceneCountInBuildSettings - 1)
		{
			PlayerPrefs.SetInt (LEVEL_KEY + level.ToString(), 1); //Using 1 like a bool. 1 == true.
		}
		else
		{
			Debug.LogError ("Trying to unlock non existant level");
		}
	}

	public static bool IsLevelUnlocked (int level)
	{
		int levelValue = PlayerPrefs.GetInt (LEVEL_KEY + level.ToString());
		bool isLevelUnlocked = (levelValue == 1);

		if (level <= SceneManager.sceneCountInBuildSettings - 1)
		{
			return isLevelUnlocked;
		}
		else
		{
			Debug.LogError ("Trying to access a level that doesn't exist in the build");
			return false;
		}
	}

	public static void SetDifficulty (float difficulty)
	{
		if (difficulty >= 1f && difficulty <= 3f)
		{
			PlayerPrefs.SetFloat (DIFFUCULTY_KEY, difficulty);
		}
		else
		{
			Debug.LogError ("Difficulty out of range");
		}
	}

	public static float GetDifficulty ()
	{
		return PlayerPrefs.GetFloat (DIFFUCULTY_KEY);
	}

}
