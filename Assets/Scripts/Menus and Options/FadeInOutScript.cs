﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInOutScript : MonoBehaviour
{
	public float FadeSpeed;

	private Image FadePanel;
	private Color currentColor = Color.black;
	// Use this for initialization
	void Start ()
	{
		FadePanel = GetComponent <Image> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Time.timeSinceLevelLoad < FadeSpeed)
		{
			float alphaChange = Time.deltaTime / FadeSpeed;
			currentColor.a -= alphaChange;
			FadePanel.color = currentColor;
		}
		else
		{
			gameObject.SetActive (false);
		}
		
	}
}
