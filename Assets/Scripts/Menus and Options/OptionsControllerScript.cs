﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OptionsControllerScript : MonoBehaviour
{
	public LevelManagerScript LevelManager;
	public Slider VolumeSlider;
	public Slider DifficultySlider;
	private MusicPlayerScript bgMusic;

	// Use this for initialization
	void Start ()
	{
		bgMusic = GameObject.FindObjectOfType <MusicPlayerScript> ();

		VolumeSlider.value = PlayerPrefsManagerScript.GetMasterVolume ();
		DifficultySlider.value = PlayerPrefsManagerScript.GetDifficulty ();
	}

	// Update is called once per frame
	void Update ()
	{
		bgMusic.ChangeVolume (VolumeSlider.value);
	}

	public void SetDefaults ()
	{
		VolumeSlider.value = 0.8f;
		DifficultySlider.value = 2f;
	}

	public void SaveAndReturn ()
	{
		PlayerPrefsManagerScript.SetMasterVolume (VolumeSlider.value);
		PlayerPrefsManagerScript.SetDifficulty (DifficultySlider.value);
		LevelManager.LoadLevel ("01a Start");
	}
}
