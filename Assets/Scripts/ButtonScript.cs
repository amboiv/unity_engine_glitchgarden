﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
	public GameObject PrefabToSpawn;
	public static GameObject SelectedGameObject;

	private ButtonScript[] buttons;
	private Text costText;
	
	// Use this for initialization
	void Start ()
	{
		buttons = FindObjectsOfType <ButtonScript> ();

		costText = GetComponentInChildren <Text> ();
		if (!costText)
		{
			Debug.LogWarning (name + " has no CostText");
		}
		costText.text = PrefabToSpawn.GetComponent <DefenderScript> ().StarCost.ToString();
	}

	// Update is called once per frame
	void Update ()
	{

	}

	void OnMouseDown ()
	{
		foreach (var button in buttons)
		{
			button.GetComponent<SpriteRenderer> ().color = Color.black;
		}
		
		GetComponent <SpriteRenderer> ().color = Color.white;
		SelectedGameObject = PrefabToSpawn;
		//SelectedGameObject.transform.position = new Vector2 (0f, 0f);
	}


}
