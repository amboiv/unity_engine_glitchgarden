﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class EnemySpawnerScript : MonoBehaviour
{
	public GameObject [] enemyPrefabs;

	private int numberOfLanes = 5;
	public float SpawnRandomizer;


	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{
		SpawnRandomizer = Random.Range (0.4f, .9f);
		ControlNewSpawns ();
	}

	bool isTimeToSpawn (GameObject attackerGameObject)
	{
		AttackerScript attacker = attackerGameObject.GetComponent <AttackerScript> ();

		float meanSpawnDelay = attacker.seenEverySeconds;
		float spawnsPerSecond = 1 / meanSpawnDelay;

		if (Time.deltaTime > meanSpawnDelay)
		{
			Debug.LogWarning ("Spawn rate capped by frame rate");
		}

		float treshold = spawnsPerSecond * Time.deltaTime / numberOfLanes;

		return (Random.value < treshold * SpawnRandomizer);
	}

	void Spawn (GameObject enemy)
	{
		GameObject newAttacker = (GameObject) Instantiate (enemy, transform.position, Quaternion.identity);
		newAttacker.transform.parent = transform;
	}

	Transform NextFreePosition ()
	{
		foreach (Transform childPositionGameObject in transform)
		{
			if (childPositionGameObject.childCount == 0)
			{
				return childPositionGameObject;
			}
		}
		return null;
	}

	void ControlNewSpawns ()
	{
		foreach (var enemy in enemyPrefabs)
		{
			if (isTimeToSpawn (enemy))
			{
				Spawn (enemy);
			}
		}
	}
}
