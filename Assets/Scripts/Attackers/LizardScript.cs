﻿using UnityEngine;
using System.Collections;

public class LizardScript : MonoBehaviour
{
	private AttackerScript attackerScript;
	private Animator animator;
	// Use this for initialization
	void Start ()
	{
		attackerScript = GetComponent<AttackerScript> ();
		animator = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update ()
	{
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		GameObject obj = other.gameObject;

		if (!obj.GetComponent<DefenderScript> ())
		{
			return;
		}
			animator.SetBool ("IsAttacking", true);
			attackerScript.Attack (obj);
	}
}
