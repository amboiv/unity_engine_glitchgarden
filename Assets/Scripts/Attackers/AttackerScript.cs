﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(HealthScript))]
public class AttackerScript : MonoBehaviour
{
	[Range (-1f, 1.5f)] public float CurrentSpeed;

	[Tooltip("Average number of seconds between appearances")]
	public float seenEverySeconds;
	//public float AttackPower;

	//private HealthScript healthScript;
	private GameObject currentTarget;
	private Animator animator;


	// Use this for initialization
	void Start ()
	{
		animator = GetComponent <Animator> ();
		//healthScript = GetComponent <HealthScript> ();
		/*Adde Rigidbody2D via script i stedet for i editoren.
		//Rigidbody2D thisRigidbody2D = gameObject.AddComponent <Rigidbody2D> ();
		//thisRigidbody2D.isKinematic = true;*/
	}

	// Update is called once per frame
	void Update ()
	{
		//Check if a target is does exist. else return to walking animation.
		if (!currentTarget)
		{
			animator.SetBool ("IsAttacking", false);
		}
		transform.Translate (Vector2.left * CurrentSpeed * Time.deltaTime);
	}

	void OnTriggerEnte2D (Collider2D other)
	{
		Debug.Log (other + "Trigger enter");
		
	}

	void SetSpeed (float speed)
	{
		CurrentSpeed = speed;
	}

	void StrikeCurrentTarget (float damage)
	{
		Debug.Log ("Damage dealth: " + damage);
		if (currentTarget)
		{
			HealthScript health = currentTarget.GetComponent<HealthScript> ();
			if (health)
			{
				health.TakeDamage (damage);
			}
		}
	}

	public void Attack (GameObject defender)
	{
		currentTarget = defender.gameObject;
	}
}
