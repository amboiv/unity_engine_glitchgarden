﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(Text))]
public class StarDisplayScript : MonoBehaviour
{
	public enum Status
	{
		SUCCESS,
		FAILURE
	};

	private Text CurrencyText;
	private int stars = 100;

	// Use this for initialization
	void Start ()
	{
		CurrencyText = GetComponent <Text> ();
		UpdateDisplay ();
		//ScoreKeeperScript.ResetScore ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void AddCurrency (int amount)
	{
		stars += amount;
		UpdateDisplay ();
	}

	public Status UseCurrency (int amount)
	{
		if (stars >= amount)
		{
			stars -= amount;
			UpdateDisplay ();
			return Status.SUCCESS;
		}
		return Status.FAILURE;

	}

	private void UpdateDisplay ()
	{
		CurrencyText.text = stars.ToString ();
	}
}
