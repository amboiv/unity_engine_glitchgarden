﻿using UnityEngine;
using System.Collections;

public class LoseColliderScript : MonoBehaviour
{
	private LevelManagerScript LevelManager;

	// Use this for initialization
	void Start ()
	{
		LevelManager = GameObject.FindObjectOfType <LevelManagerScript> ();
	}

	// Update is called once per frame
	void Update ()
	{
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		LevelManager.LoadLevel ("03b Lose");	
	}
}
