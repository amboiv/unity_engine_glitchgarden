﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTimerScript : MonoBehaviour
{
	
	[Tooltip("Set starting time in Seconds")]
	public float StartingTime = 60f;

	private AudioSource levelClearSound;
	private LevelManagerScript levelManager;
	private float remainingTime;
	private Slider slider;
	private bool isEndOfLevel = false;
	private GameObject LevelClearText;

	void Awake ()
	{
		remainingTime = StartingTime;
	}

	// Use this for initialization
	void Start ()
	{
		levelClearSound = GetComponent <AudioSource> ();
		levelManager = GameObject.FindObjectOfType<LevelManagerScript> ();
		slider = GetComponent <Slider> ();
		LevelClearText = GameObject.Find ("LevelClearText");
		FindAndHideLevelClearText ();

		
	}

	private void FindAndHideLevelClearText ()
	{
		if (!LevelClearText)
		{
			Debug.LogWarning (name + "The LevelCLearText Could not be found. Does it exist?");
		}
		LevelClearText.SetActive (false);
	}

	// Update is called once per frame
	void Update ()
	{
		if (remainingTime <= 0 && !isEndOfLevel)
		{
			HandleWinCondition ();
		}
		remainingTime -= Time.deltaTime;
		UpdateSlider ();
	}

	private void HandleWinCondition ()
	{
		DestroyAllTaggedObjects();
		levelClearSound.Play ();
		LevelClearText.SetActive (true);
		Invoke ("LoadNextLevel", levelClearSound.clip.length);
		isEndOfLevel = true;
	}

	void DestroyAllTaggedObjects ()
	{
		GameObject [] destroyables = GameObject.FindGameObjectsWithTag ("DestroyOnWin");
		foreach (var obj in destroyables)
		{
			Destroy (obj);
		}
	}

	void UpdateSlider ()
	{
		slider.value = 1 - (remainingTime / StartingTime);
	}

	void LoadNextLevel ()
	{
		levelManager.LoadNextLevel ();
	}
}
